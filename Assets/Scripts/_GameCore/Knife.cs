﻿using DG.Tweening;
using System;
using UnityEngine;

namespace canmujde 
{
    public class Knife : MonoBehaviour, IResetable
    {
        private bool isMoving;
        private bool inputActive;
        [SerializeField]private float sensitivity;
        private Vector3 lastMousePoint;
        private Vector3 mouseOffset;
        private Vector3 translation;
        public bool IsMoving { get => isMoving; set => isMoving = value; }

        [SerializeField] public float minZ, maxZ;

        [SerializeField] Vector3[] soapsLayerLocalPositions;
        [SerializeField] GameObject mask;
        [SerializeField] Transform steel;
        int currentLayer;
        Vector3 maskOffset = new Vector3(-0.2f, 0.08f, 0.7f);

        bool inSoap;


        #region Unity
        private void OnEnable()
        {
            Reset();
        }

        private void Update()
        {
            GetInput();
        }
        #endregion

        //reset behaviour
        public void Reset()
        {
            ContentManager.Instance.currentKnife = this;
            translation = Vector3.zero;
            IsMoving = false;
            inputActive = true;
            currentLayer = 0;
            mask.SetActive(true);
            mask.transform.SetParent(GameManager.instance.LevelManager.levelParent);
            mask.transform.position = transform.position + maskOffset;
        }

        

        //getting input
        private void GetInput()
        {
            if (!inputActive || GameManager.CurrentState!= GameState.Ingame) return;

            if (Input.GetMouseButtonDown(0))
            {
                Move();
            }
            if (Input.GetMouseButtonUp(0))
            {
                IsMoving = false;

            }
            //apply movement
            if (IsMoving)
            {
                mouseOffset = (Input.mousePosition - lastMousePoint);
                translation.z  = mouseOffset.y;

                

                Vector3 to = transform.localPosition + translation;

                if ( transform.position.z < mask.transform.position.z - 0.75f)
                {
                    mask.transform.position = transform.position+ maskOffset;
                    if (inSoap)
                    ContentManager.Instance.currentSoap.PlayParticle(steel);
                }

                transform.localPosition =  Vector3.Lerp(transform.localPosition, to, Time.deltaTime*sensitivity );


                Clamp();

                lastMousePoint = Input.mousePosition;
            }
        }
        //clamping position
        private void Clamp()
        {
            Vector3 localPos = transform.localPosition;
            localPos.z = Mathf.Clamp(localPos.z, minZ, maxZ);
            localPos.y = Mathf.Clamp(localPos.y, minZ, maxZ);
            transform.localPosition = localPos;
        }
        //confirm movement
        public void Move()
        {
            IsMoving = true;
            lastMousePoint = Input.mousePosition;
        }


        private void OnTriggerStay(Collider other)
        {
            if (other.CompareTag("Soap"))
                inSoap = true;
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Soap"))
                inSoap = false;
        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("SoapFinish"))
            CheckIsFinished();
        }

        private void CheckIsFinished()
        {
            mask.SetActive(false);
            currentLayer++;
            ContentManager.Instance.currentSoap.SetLayers(currentLayer);
            
            if (currentLayer< soapsLayerLocalPositions.Length)
            {
                transform.DOLocalJump(soapsLayerLocalPositions[currentLayer], 1, 1, 1).OnComplete(() => 
                {
                    inputActive = true;
                    mask.SetActive(true);
                    mask.transform.position = transform.position + maskOffset;
                });
                
                
            }
            else
            {
                transform.DOLocalJump(soapsLayerLocalPositions[2], 1, 1, 1).OnComplete(() => GameManager.instance.ChangeState(GameState.Win));


            }
            
        }


    }

}
