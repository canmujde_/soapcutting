using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace canmujde
{

    public class SoapLayer : MonoBehaviour
    {
        [SerializeField] private Renderer rend;

        [SerializeField] private Material maskMaterial;
        [SerializeField] private Material normalMaterial;
        

        public void Reset(bool isFirstLayer)
        {
            rend.material = isFirstLayer ? maskMaterial : normalMaterial;
        }

        public void SetMaterial(bool isMask)
        {
            rend.material = isMask ? maskMaterial : normalMaterial;
        }
    }

}
