using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace canmujde
{

    public class Soap : MonoBehaviour, IResetable
    {

        [SerializeField] private SoapLayer[] layers;

        public ParticleSystem cutParticle;

        private void OnEnable()
        {
            Reset();
        }

        public void Reset()
        {
            ContentManager.Instance.currentSoap = this;
            for (int i = 0; i < layers.Length; i++)
            {
                layers[i].gameObject.SetActive(true);
                if (i == 0)
                    layers[i].Reset(true);
                else
                    layers[i].Reset(false);
            }


        }

        public void SetLayers(int activeLayerId)
        {
            for (int i = 0; i < layers.Length; i++)
            {
                if (i< activeLayerId)
                {
                    layers[i].gameObject.SetActive(false);
                }
                if (i == activeLayerId)
                {
                    layers[i].SetMaterial(true);
                }
            }
        }

        internal void PlayParticle(Transform t)
        {
            Vector3 pos = t.position;
            pos.x = transform.position.x;
            cutParticle.transform.position = pos;
            cutParticle.Play();
        }
    }

}