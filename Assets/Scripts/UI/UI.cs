﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace canmujde
{

    public class UI : MonoBehaviour, IUI
    {
        public GameState State;

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }


    }

}