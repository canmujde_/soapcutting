﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
namespace canmujde
{

    public class LevelEditorHelper : MonoBehaviour
    {

        public int LevelID;
        public LevelManager levelManager;
        public PoolManager poolManager;
        public List<Transform> CurrentObjects;

#if UNITY_EDITOR
        [Button]
        public void SaveLevel()
        {

            if (FindObjectsOfType<O_PrefabInfo>().Length <= 0)
            {
                Debug.LogWarning("There is no prefab to save to level data in the current scene.");
                return;
            }

            Level asset = levelManager.Levels.FirstOrDefault(lstLevel => lstLevel.LevelID == LevelID);

            bool newObject = false;
            if (asset == null)
            {
                newObject = true;
                asset = ScriptableObject.CreateInstance<Level>();
            }

            O_PrefabInfo[] saveObjects = FindObjectsOfType<O_PrefabInfo>();
            asset.LevelDatas = new List<LevelData>();

            for (int i = 0; i < saveObjects.Length; i++)
            {
                LevelData levelData = new LevelData
                {
                    PrefabID = saveObjects[i].prefabID,
                    TransformData = saveObjects[i].GetComponent<O_TransformData>().GetData()
                };
                asset.LevelDatas.Add(levelData);
            }

            asset.LevelID = LevelID;

            if (newObject)
            {
                UnityEditor.AssetDatabase.CreateAsset(asset, $"Assets/Data/Levels/{LevelID}.asset");

                List<Level> levels = levelManager.Levels.ToList();

                levels.Add(asset);

                levelManager.Levels = levels.ToArray();

            }

            UnityEditor.EditorUtility.SetDirty(asset);



            foreach (var item in saveObjects)
            {
                DestroyImmediate(item.gameObject);
            }

            CurrentObjects.Clear();
            Debug.Log("Saved level as Level ID: " + asset.LevelID);
        }


        [Button]
        public void LoadLevel()
        {
            CreateLevel(LevelID);
        }

        [Button]
        public void ClearLevel()
        {
            for (int i = CurrentObjects.Count - 1; i >= 0; i--)
            {
                DestroyImmediate(CurrentObjects[i].gameObject);
            }
            CurrentObjects.Clear();
        }
        public void CreateLevel(int levelID)
        {
            if (CurrentObjects.Count > 0)
                ClearLevel();
            Level current = GetLevel(levelID);
            if (current)
            {
                SetLevel(current);
            }
        }

        public Level GetLevel(int levelID)
        {
            Level level = levelManager.Levels.FirstOrDefault(lstLevel => lstLevel.LevelID == levelID);

            if (level == null)
            {
                level = levelManager.Levels.FirstOrDefault(lstLevel => lstLevel.LevelID == 0);
                Debug.Log("levelId value is not in the list.");
            }

            return level;
        }

        private void SetLevel(Level current)
        {

            for (int i = 0; i < current.LevelDatas.Count; i++)
            {
                Transform instance = poolManager.PrefabData.PrefabDataList.Find(x => x.prefab.prefabID == current.LevelDatas[i].PrefabID).prefab.transform;

                GameObject instantiate = Instantiate(instance.gameObject);


                instantiate.transform.parent = transform;
                instantiate.GetComponent<O_TransformData>().SetData(current.LevelDatas[i].TransformData);

                CurrentObjects.Add(instantiate.transform);

            }


        }
#endif



    }

}