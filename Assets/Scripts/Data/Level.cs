
using System.Collections.Generic;
using UnityEngine;

namespace canmujde
{


    [CreateAssetMenu(fileName = "LevelData", menuName = "Create Level", order = 0)]
    public class Level : ScriptableObject
    {
        public int LevelID = 0;

        public List<LevelData> LevelDatas;

    }



    [System.Serializable]
    public class LevelData
    {
        public int PrefabID;

        public TransformData TransformData;
    }
}