﻿
using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

namespace canmujde
{

    [CreateAssetMenu(fileName = "GamePrefabsData", menuName = "Create Game Prefabs Data", order = -1)]
    public class GamePrefabsData : ScriptableObject
    {

        public List<PrefabData> PrefabDataList;


        private void OnValidate()
        {
            for (int i = 0; i < PrefabDataList.Count; i++)
            {
                PrefabDataList[i].id = PrefabDataList[i].prefab.prefabID;
            }
        }

    }

    [System.Serializable]
    public class PrefabData
    {
        public int id;
        public O_PrefabInfo prefab;
        public int size;
    }




}
