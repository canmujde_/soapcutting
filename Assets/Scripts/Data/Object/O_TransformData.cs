﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace canmujde
{

    [RequireComponent(typeof(O_PrefabInfo))]
    public class O_TransformData : MonoBehaviour
    {

        public TransformData GetData()
        {
            return new TransformData(transform.localPosition, transform.localRotation, transform.localScale);
        }

        public void SetData(TransformData data)
        {
            SetTransformData(data);
        }

        private void SetTransformData(TransformData data)
        {
            transform.localPosition = data.Position;
            transform.localRotation = data.Rotation;
            transform.localScale = data.Scale;

        }

    }


    [System.Serializable]
    public class TransformData
    {
        public Vector3 Position;
        public Quaternion Rotation;
        public Vector3 Scale;

        public TransformData(Vector3 position, Quaternion rotation, Vector3 scale)
        {
            Position = position;
            Rotation = rotation;
            Scale = scale;
        }
    }
}