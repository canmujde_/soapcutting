using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace canmujde
{
    public class ContentManager : MonoBehaviour
    {

        public static ContentManager Instance;

        public Knife currentKnife;
        public Soap currentSoap;

        private void Awake()
        {
            Instance = this;
        }

    }

}
