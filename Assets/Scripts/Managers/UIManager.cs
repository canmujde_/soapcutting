﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
namespace canmujde
{

    public class UIManager : MonoBehaviour
    {
        public UI[] UIS;
        public Text currentLevelText;

        private void Update()
        {
            if (GameManager.CurrentState != GameState.Ingame) return;
        }


        public void ChangeUI(GameState state)
        {
            Array.ForEach(UIS, x =>
            {
                if (x.State == state) x.Show();
                else x.Hide();
            });
        }

        public void NextLevelButton_OnClick()
        {
            GameManager.instance.ChangeState(GameState.Menu);
            //LevelManager.instance.RespawnLevel();  
        }

        public void PlayButton_OnClick()
        {
            GameManager.instance.ChangeState(GameState.Ingame);
        }

        public void RestartButton_OnClick()
        {
            GameManager.instance.ChangeState(GameState.Menu);
            //LevelManager.instance.RespawnLevel();
        }


        public void UpdateLevelText()
        {
            currentLevelText.text = "LEVEL " + (GameManager.instance.LevelManager.CurrentLevel).ToString();
        }

    }

}