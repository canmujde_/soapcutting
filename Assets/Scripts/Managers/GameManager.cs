﻿using System;
using UnityEngine;

namespace canmujde
{

    public class GameManager : MonoBehaviour
    {
        public static GameManager instance;


        public static GameState CurrentState;

        public static Action OnLevelLoaded;
        public static Action OnLevelStarted;
        public static Action<GameResult> OnLevelEnded; //it's all public for if any script wants to subscribe


        private UIManager uIManager;
        private LevelManager levelManager;
        private PoolManager poolManager;

        public bool logStateChanges;




        #region Unity
        private void Awake()
        {
            instance = this;
            OnLevelLoaded += onLevelLoaded;
            OnLevelStarted += onLevelStarted;
            OnLevelEnded += onLevelEnded;

            uIManager = GetComponent<UIManager>();
            levelManager = GetComponent<LevelManager>();
            poolManager = GetComponent<PoolManager>();
        }

        private void Start()
        {

            poolManager.CreatePool();
            ChangeState(GameState.Menu);
        }

        private void Update()
        {
#if UNITY_EDITOR
            ManipulateGame();
#endif
        }


        #endregion

        private void ManipulateGame()
        {
            #region State Manipulation
            if (Input.GetKeyDown(KeyCode.W))
            {
                ChangeState(GameState.Win);
            }
            if (Input.GetKeyDown(KeyCode.F))
            {
                ChangeState(GameState.Fail);
            }
            #endregion
            #region Time Manipulation

            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                Time.timeScale = 1;
            }
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                Time.timeScale = 2;
            }
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                Time.timeScale = 3;
            }
            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                Time.timeScale = 4;
            }
            if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                Time.timeScale = 5;
            }

            #endregion
        }
        public void ChangeState(GameState newState)
        {
            CurrentState = newState;

            switch (CurrentState)
            {
                case GameState.Menu:
                    OnLevelLoaded?.Invoke();
                    break;
                case GameState.Ingame:
                    OnLevelStarted?.Invoke();
                    break;
                case GameState.Fail:
                    OnLevelEnded.Invoke(GameResult.Fail);
                    break;
                case GameState.Win:
                    OnLevelEnded.Invoke(GameResult.Win);
                    break;
            }
            UIManager.ChangeUI(CurrentState);
        }

        #region Events
        private void onLevelLoaded()
        {
            levelManager.CreateLevel();
            uIManager.UpdateLevelText();
            if (logStateChanges)
                Debug.Log("OnLevelLoaded");
        }

        private void onLevelStarted()
        {
            if (logStateChanges)
                Debug.Log("OnLevelStarted");
        }

        private void onLevelEnded(GameResult result)
        {

            if (logStateChanges)
                Debug.Log("OnLevelEnded Result: " + result);

            switch (result)
            {
                case GameResult.Win:
                    levelManager.LevelSucceeded();
                    break;
                case GameResult.Fail:

                    break;
            }
        }
        #endregion

        public UIManager UIManager { get => uIManager; }
        public LevelManager LevelManager { get => levelManager; }
        public PoolManager PoolManager { get => poolManager; }
    }



    public enum GameState
    {
        Menu,
        Ingame,
        Fail,
        Win
    }
    public enum GameResult
    {
        Win,
        Fail
    }






}