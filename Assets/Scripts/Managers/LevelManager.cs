﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;



namespace canmujde
{

    public class LevelManager : MonoBehaviour
    {
        public Level[] Levels;

        private int currentLevelId;
        private int currentLevel;
        public Level _currentLevel;

        public Transform levelParent;

        [Tooltip(Constants.REPEAT_LAST_LEVEL_TOOLTIP)]
        public int repeatLastLevels;

        public List<Transform> CurrentObjects;

        private void Awake()
        {
            GetLevelProgression();
        }

        private void GetLevelProgression()
        {
            currentLevel = PlayerPrefs.GetInt("currentLevel", 1);
            currentLevelId = PlayerPrefs.GetInt("currentLevelId", 0);
        }

        public void CreateLevel()
        {
            ClearLevel();
            Level current = GetLevel(currentLevelId);
            if (current)
            {
                SetLevel(current);
            }

        }

        public void SetLevel(Level current)
        {
            _currentLevel = current;
            for (int i = 0; i < _currentLevel.LevelDatas.Count; i++)
            {


                O_PrefabInfo pooledObject = GameManager.instance.PoolManager.GetFromPool(_currentLevel.LevelDatas[i].PrefabID);


                GameManager.instance.PoolManager.pooledObjects.Remove(pooledObject);
                pooledObject.transform.parent = levelParent;


                #region Data Load
                pooledObject.GetComponent<O_TransformData>().SetData(_currentLevel.LevelDatas[i].TransformData);


                #endregion


                pooledObject.gameObject.SetActive(true);
                CurrentObjects.Add(pooledObject.transform);

            }
        }

        public Level GetLevel(int levelID)
        {
            Level level = Levels.FirstOrDefault(lstLevel => lstLevel.LevelID == levelID);

            if (level == null)
            {
                currentLevelId = repeatLastLevels > 0 ? Levels.Length - repeatLastLevels : 0;
                level = Levels.FirstOrDefault(l => l.LevelID == currentLevelId);
            }

            return level;
        }

        public void ClearLevel()
        {
            for (int i = 0; i < CurrentObjects.Count; i++)
            {
                GameManager.instance.PoolManager.RePoolObject(CurrentObjects[i].GetComponent<O_PrefabInfo>());
            }

            CurrentObjects.Clear();
        }

        public void LevelSucceeded()
        {
            currentLevel++;
            currentLevelId++;
            PlayerPrefs.SetInt("currentLevelId", currentLevelId);
            PlayerPrefs.SetInt("currentLevel", CurrentLevel);
        }







        public int CurrentLevel
        {
            get { return currentLevel; }
        }




    }

}
