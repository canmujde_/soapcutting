﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace canmujde
{

    public class PoolManager : MonoBehaviour
    {

        public GamePrefabsData PrefabData;

        public Transform poolParent;
        public List<O_PrefabInfo> pooledObjects;

        public void CreatePool()
        {
            foreach (PrefabData prefabData in PrefabData.PrefabDataList)
            {

                for (int i = 0; i < prefabData.size; i++)
                {
                    GameObject obj = Instantiate(prefabData.prefab.gameObject);
                    O_PrefabInfo prefabInfo = obj.GetComponent<O_PrefabInfo>();
                    obj.SetActive(false);
                    obj.transform.SetParent(poolParent);
                    pooledObjects.Add(prefabInfo);
                }

            }
        }

        public void RePoolObject(O_PrefabInfo gameObjectToPool)
        {
            pooledObjects.Add(gameObjectToPool);
            gameObjectToPool.transform.SetParent(poolParent);
            gameObjectToPool.gameObject.SetActive(false);
        }

        public O_PrefabInfo GetFromPool(int objectID)
        {

            try
            {
                return pooledObjects.Find(obj => obj.prefabID == objectID);

            }
            catch (System.Exception)
            {
                //Maybe increase pool size?
                Debug.Log($"No object in pool with id : {objectID} ");
                return null;

            }


        }


    }

}