
namespace canmujde
{

    public interface ICollectable
    {
        void OnCollect();
    }

}