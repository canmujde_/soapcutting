﻿
using UnityEngine;

namespace canmujde
{

    public interface IResetable
    {
        void Reset();
    }

}