﻿using UnityEngine;

namespace canmujde
{

    public static class Constants
    {

        public const string REPEAT_LAST_LEVEL_TOOLTIP = "'0' means the game will loop all levels. \nOtherwise the game will loop last 'n' levels.";

    }

}
